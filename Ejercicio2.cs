using System;

namespace Ejercicio2
{
    class Empleado
    {
        private string[] nombre;
        private float[] sueldos;

        public void Cargar()
        {
            nombre = new string[5];
            sueldos = new float[5];
            for (int i = 0; i < nombre.Length; i++)
            {
                Console.Write("Favor indicar su nombre: \n");
                nombre[i] = Console.ReadLine();
                Console.Write("Ingrese su sueldo: \n");
                string ConvertSueldo;
                ConvertSueldo = Console.ReadLine();
                sueldos[i] = float.Parse(ConvertSueldo);
            }
        }
        public void SueldoMayor()
        {
            float Smayor;
            int pos;
            Smayor =sueldos[0];
            pos = 0;
            for (int i = 0; i <nombre.Length; i++)
            {
                if(sueldos[i] > Smayor)
                {
                    Smayor = sueldos[i];
                    pos = i;
                }
            }
            Console.Write("El empleado con el sueldo mayor es: "+nombre[pos]+" tiene un salario es equivalente a: "+Smayor);
        }

        static void Main(string[] args)
        {
            /* Crear un programa que permita cargar los nombres de 5 empleados y sus salarios respectivos. Mostrar el salario mayor y el nombre del empleado */
            Empleado ep = new Empleado();
            ep.Cargar();
            ep.SueldoMayor();
        }
    }
}
